import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { LazyLog } from 'react-lazylog';

/**
 * ExampleComponent is an example component.
 * It takes a property, `label`, and
 * displays it.
 * It renders an input with the property `value`
 * which is editable by the user.
 */
export default class DashLazylog extends Component {
    render() {
        const {props} = this.props;

        return (
            <LazyLog
                extraLines={1}
                enableSearch
                selectableLines
                caseInsensitive
                {...props}
            ></LazyLog>
        );
    }
}

DashLazylog.defaultProps = {};

DashLazylog.propTypes = {
    /**
     * Properties to pass onto react-lazylog. See:
     * https://mozilla-frontend-infra.github.io/react-lazylog/
     */
    props: PropTypes.object,

    /**
     * Dash-assigned callback that should be called to report property changes
     * to Dash, to make them available for callbacks.
     */
    setProps: PropTypes.func
};
