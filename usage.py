import dash_lazylog
import dash
from dash.dependencies import Input, Output
import dash_html_components as html

app = dash.Dash(__name__)

app.layout = html.Div([
    dash_lazylog.DashLazylog(
        props={
            'width': 'auto',
            'height': 500,
            'url':'https://gist.githubusercontent.com/helfi92/96d4444aa0ed46c5f9060a789d316100/raw/ba0d30a9877ea5cc23c7afcd44505dbc2bab1538/typical-live_backing.log'
        }
    ),
    html.Div(id='output')
])


if __name__ == '__main__':
    app.run_server(debug=True)
